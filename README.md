# Development Tool - Timber

A development process for Shopify based on [Gulp](https://gulpjs.com/) that helps local theme development and automates tasks like, theme download, live watch of file changes, concatenation and minification of .scss and .js files, deployment of local files to specific Shopify theme ID and many more.

It uses the latest Timber theme as the main Skeleton theme.

---

**Table of Contents**

- [Supported Features](#markdown-header-supported-features)
- [Project Structure](#markdown-header-project-structure)
- [Getting Started](#markdown-header-getting-started)
- [Using the Tool](#markdown-header-using-the-tool)
- [Important Notes](#markdown-header-important-notes)

---

## Supported Features
**SASS Automatic Actions**: We are using [PostCSS](http://postcss.org/) to add features like: vendor prefixes, automatic import of Google fonts.

**Javascript Concatenation and Minification**: Multiple external .js files can be automatically compiled and minified under one file. This ensures good loading times of the theme and helps developers in the team work under the same theme scaffolding.

**Effective Development Flow**: When working locally, every time you make changes to any file (.liquid, .js, scss, assets) the files are automatically processed and uploaded to your specified theme.

**Autoload of Browsers During Development**: Browsers respond automatically to file changes and auto-reload to preview the changes. This works for multiple devices as well. E.g. you can have a mobile device next to your desktop PC and all browsers in all devices will reload automatically whenever making local changes.

**Build-in Commands for Downloading and Deployment**: Console commands for downloading and uploading themes to Shopify.

## Getting Started

### Prerequisites
You must have both node.js and the node package manager (npm) installed in your system. You can download both from https://nodejs.org/en/

### Download Files
To start a new project, you will need to download this repository and extract the files in your project folder.

### Install Dependencies
Open your terminal/console pointing at your project folder and  run `npm install`. This will install all the dependencies from the local `package.json` file.

### Setup your Shopify environments
Once you have bootstraped your development theme, you will need to setup your `config.yml` file with the right information. Rename the `config-sample.yml` to `config.yml`. Open it and edit with the appropriate settings.

We will refer to Shopify's documentation on how to give API access and where to find the `api_key`,`password`, `theme_id`, `store` necessary for the CLI to interact with the Shopify servers: https://github.com/Shopify/slate-cli/blob/master/store-configuration.md.

It is strongly recommended to use two themes. One that will serve as your development environment and another one as your production.

## Project Structure
Once WPF has created the scaffolding of your project, it will have the following structure:

```
├── gulpfile.js
├── config-sample.yml
├── package.json
├── node_modules
├── downloads
    ├── theme-dev
    └── theme-prod
└── dev
    ├── js
    └── scss
└── shop
    ├── assets
    ├── config
    ├── layout
    ├── locales
    ├── sections
    ├── snippets
    ├── specs
    └── templates
```

#### Custom Theme JS Functionality

`dev/js/theme.js`

All of your additional javascript functions and custom functionality in general must be placed at this file. Please avoid having scattered javascript code inside .liquid files. This should be avoided at all costs. Having one central .js file that holds all of the custom site functionality has many advantages for many obvious reasons.

Liquid templating is allowed in your `dev/js/theme.js` file al long as you escape the liquid tags as string e.g.
```
var mainColor = "{{ settings.main_color }}"
```

#### Vendors & Plugins

`dev/js/plugins`

All javascript and jQuery plugins .js files should be placed at this location. All .js files will be picked-up automatically, concatenated and minified under one file as `plugins.min.js`. This final file will be exported automatically at the `shop/assets` folder. This file loads at the `shop/layout/theme.liquid` file before the closing `body` tag.

#### Sass and CSS Files

`dev/scss/theme.scss`

All your custom styling and css rules must be placed here. The build process fully supports partial imports e.g.
```
import '../sass/header.scss';
```

Vendor prefixes will be generated automatically for you so you don't have to write additional styles with browser prefixes. E.g.

```
.foo{
  transform: tranlate3d(0,0,0,)
}
```

will be converted automatically to

```
.foo{
  -webkit-transform: translate 3d(0,0,0);
      -ms-transform: translate 3d(0,0,0);
          transform: translate 3d(0,0,0)
}
```

We are using a PostCSS plugin to bring some magic to Google fonts import. If you want to, you don't need to explicitly import your Google fonts. E.g. the following style

```
.foo{
  font-family: 'Roboto', sans-serif;
}
```

will import all Google fonts at the built file automatically. So this font declaration will work outside the box.

Note. If you are using multiple fonts the aforementioned way is not always the best way to bring your fonts instead you should use combined font imports or using asynchronous methods for fonts loading.


#### Shopify Required

- `src/config`
- `src/layout/theme.liquid`
- `src/locales`
- `src/sections`
- `src/snippets`
- `src/templates/*.liquid`

The aforementioned [files and folders are required by Shopify](https://help.shopify.com/themes/development/templates) for any given theme.


## Using the Tool

### API Commands
Here are the available API commands:

`npm run serve`

  - Builds and uploads files from the `dev` folder to Shopify configured development theme
  - Starts the internal server,
  - Will serve on `https://localhost:3000`

Note: This works only for published themes. If the theme is not published you should manually load the preview URL in your browser.

`npm run deploy-dev`

  - Uploads all of your local theme files from the `shop` folder to Shopify at your development theme

`npm run deploy-prod`

  - Uploads all of your local theme files from the `shop` folder to Shopify at your production theme

`npm run download-dev`

  - Downloads all files from your Shopify development theme into the `downloads/theme-dev` folder

`npm run download-prod`

  - Downloads all files from your Shopify production theme into the `downloads/theme-prod` folder

## Important Notes
### Browser Auto-load and Local Preview of Themes
This works only for published themes. These themes can be proxied under `https://localhost:3000`.

If the theme is not published you should manually load the theme's preview URL in your browser.
