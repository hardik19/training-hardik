var eatFeedLove = {
	init: function () {
		this.topBar();
		this.bxslider();
		this.cart();
		this.search();	
		this.flag();
		this.menu();
		this.filter();
	},
	topBar: function(){
		// charity count 
		$('.charity_count').click(function(e) {
			$('.People-fed-dropdown').slideToggle();
		});
	},
	bxslider: function(){
		// ========================= Home Page Main Slider Trigger
		$('.bxslider').bxSlider({
			mode: 'fade',
			captions: true
		});
	},
	cart:function(){
		// ========================= header cart
		$('.cart_icon').click(function(e) {
			$('.remort-cart').slideToggle();
			$('.overlay').toggle();
		});
		$('.close-icon button').click(function(e) {
			$('.remort-cart').slideToggle();
			$('.overlay').toggle();
		});
	},
	search:function(){
		// Header search 
		$('.search_icon').click(function(e) {
			e.preventDefault();
			
			$(this).hide('fast');
			$('.search-form').show('fast');
		});
		$('.search-form input[type="submit"]').click(function(e) {
			var get_text = $('.search-form input[type="text"]').val();
			if(!get_text){
				e.preventDefault();
				// Set the effect type
				$('.search_icon').toggle();
				$('.search-form').toggle();
			}
			else{
				return true;
			}
		});
	},
	flag:function(){
		//Flag change
		$('.flag').click(function(e) {
			$('.select-flag').slideToggle();
		});
		$('.select-flag ul li').each(function(index, element) {
			$(this).click(function(e) {
				$(this).siblings().removeClass('active');
				$(this).addClass('active');
				var get_flag = $('img',this).attr('src');
				$('.curruncy_change .flag-icon').attr('src',get_flag);
				$('.select-flag').slideToggle();
			});
		});
	},
	menu:function(){
		// Responsive menu 
		jQuery('header nav').meanmenu();
			$(".meanmenu-reveal").click(function(){
			$(".mean-nav").toggleClass("intro");		
		});
	},
	filter:function(){
		// ========================= recipes filter
		$('.filter_icon').click(function(e) {
			$('.remort-filter').slideToggle();
		});

		$('.filter_close-icon button').click(function(e) {
			$('.remort-filter').slideToggle();
		});
	}
}
$(document).ready(function () {
	eatFeedLove.init();
});


jQuery(function($){
	var windowWidth = $(window).width();
		$(window).resize(function() {
		if(windowWidth != $(window).width()){
		location.reload();
		return;
		}
	});
 });


// =========================
$('html').click(function() {
	$('.People-fed-dropdown').slideUp();
	$('.search_icon').show('fast');
	$('.search-form').hide('fast');
	$('.select-flag').slideUp();
	$('.remort-cart').slideUp();
	$('.overlay').hide();
	$('.remort-filter').slideUp();
});

$('.charity_count,.People-fed-dropdown,.search-form,.search_icon,.flag,.select-flag,.remort-cart,.cart_icon,.filter_icon').click(function(event){
    event.stopPropagation();
});

// ========================= Replace SVG images to normal
if (!jQuery.support.leadingWhitespace){
	//IE 7-8
}
else if ($.browser.msie  && parseInt($.browser.version, 10) > 8) {
	//IE 9-10
} 
else if (!!navigator.userAgent.match(/Trident\/7\./)){
  	// IE 11
}

//====================== product details

if (jQuery(window).width() <= 767) {
	$('.products_main').bxSlider({
	  minSlides: 1,
	  maxSlides: 1,
	  slideWidth:767,	
	  pager: false,	
	  auto: true,
	  controls: false,
	});
}
else if(jQuery(window).width() >= 768){
	$('.products_main').bxSlider({
	pagerCustom: '#bx-pager',
	mode: 'fade',
	captions: true
});
}

//====================== product filter

$(window).load(function () {
	var $container = $('#items');
	$container.imagesLoaded(function () {
		$container.isotope({
		   itemSelector: '.item',
		   layoutMode: 'fitRows',		   
		});
	});
	$('.filter_nav li a').click(function () {
		$('.filter_nav li a').removeClass('active');
		$(this).addClass('active');
		var selector = $(this).attr('data-filter');
		$container.isotope({
			filter: selector
		});
		return false;
	});
});

//=================================================  mobile product slider  ======================


    if (jQuery(window).width() <= 479) {
		jQuery('.thumb_slider .product_list').addClass('mobile_product');	
		$('.mobile_product').bxSlider({
		  minSlides: 1,
		  maxSlides: 1,
		  slideWidth: 310,	
		  pager: false,	
		  auto: true,
		  controls: false,
		});
	}
	else if(jQuery(window).width() >= 768){
		jQuery('.thumb_slider .product_list').removeClass('mobile_product');
	}
	
//==========================================  cetegories silder  

if (jQuery(window).width() <= 479) {
	jQuery('#product_cetegories .cetegories ul').addClass('cetegories_slider');
	$('.cetegories_slider').bxSlider({
		minSlides: 2,
		maxSlides: 8,
		slideWidth:100,
		slideMargin:30,
		pager: false,	
		auto: true,
		controls: false,
	});
}

else if (jQuery(window).width() >= 480 && jQuery(window).width() <= 767 ) {
	jQuery('#product_cetegories .cetegories ul').addClass('cetegories_slider');
	$('.cetegories_slider').bxSlider({
		minSlides: 2,
		maxSlides: 8,
		slideWidth:100,
		slideMargin:30,
		pager: false,	
		auto: true,
		controls: false,
	});
}
else if (jQuery(window).width() >= 768 && jQuery(window).width() <= 1170 ) {
	jQuery('#product_cetegories .cetegories ul').addClass('cetegories_slider');
	$('.cetegories_slider').bxSlider({
		minSlides: 2,
		maxSlides: 8,
		slideWidth:100,
		slideMargin:30,
		pager: false,	
		auto: true,
		controls: false,
	});
}
else if(jQuery(window).width() >= 1170){
	jQuery('#product_cetegories .cetegories ul').removeClass('cetegories_slider');
}
	
	
	
	

$('.mb_menu_title .close_arrow').click(function(e) {
    $('.mean-container .mean-nav.intro > ul').hide('100');
	$('.mean-container .mean-bar a').removeclass("meanclose");
});



	
	