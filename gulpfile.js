/* gulpfile.js */
var YAML = require('yamljs');
var config = YAML.load('./config.yml');

var
  gulp = require('gulp'),
  sass = require('gulp-sass'),
  postcss = require('gulp-postcss'),
  autoprefixer = require('autoprefixer'),
  cssnano = require('cssnano'),
  fontmagician = require('postcss-font-magician'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  gulpShopify = require('gulp-shopify-upload-with-callbacks'),
  watch = require('gulp-watch'),
  notify = require('gulp-notify'),
  plumber = require('gulp-plumber'),
  browserSync = require('browser-sync').create(),
  reload = browserSync.reload;

// source and distribution folders
var
  source = './dev/',
  dest = './shop/';

var onError = function(err) {
  notify.onError({
    title: "Gulp",
    subtitle: "Failure!",
    message: "Error: <%= error.message %>",
    sound: "Beep"
  })(err);

  this.emit('end');
};

gulp.task('browserSync', function() {
  browserSync.init({
    proxy: 'https://' + config.development.store,
    notify: true,
    ghostMode: {
      clicks: true,
      scroll: true,
      links: true,
      forms: true
    }
  });
});

gulp.task('theme-styles', function() {
  var processors = [
    fontmagician,
    autoprefixer({
      browsers: ['last 2 versions']
    }),
    cssnano({
      discardComments: {
        removeAll: true
      }
    })
  ];
  return gulp.src(source + 'scss/theme.scss')
    .pipe(plumber({
      errorHandler: onError
    }))
    .on("error", notify.onError({
      message: "Error: <%= error.message %>",
    }))
    .pipe(postcss(processors))
    .pipe(rename('theme.scss.liquid'))
    .pipe(gulp.dest(dest + 'assets/'))
    .pipe(notify({
      message: "Theme CSS ready",
      onLast: true
    }));
});

// jQuery plugins
gulp.task('theme-plugins-js', function() {
  return gulp.src(source + 'js/plugins/*.js')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(concat('plugins.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(dest + 'assets/'))
    .pipe(notify({
      message: "Theme Plugins JS ready",
      onLast: true
    }));
});

// functions.js
gulp.task('theme-js', function() {
  return gulp.src(source + 'js/theme.js')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(rename('theme.min.js.liquid'))
    .pipe(uglify())
    .pipe(gulp.dest(dest + 'assets/'))
    .pipe(notify({
      message: "Theme JS file ready",
      onLast: true
    }));
});

gulp.task('shopify', ['browserSync'], function() {
  return watch(dest + '**/*')
    .pipe(gulpShopify(config.development.api_key, config.development.password, config.development.store, config.development.theme_id, {
      'basePath': dest
    }))
    .pipe(browserSync.stream());
});

gulp.task('watch', function() {
  gulp.watch(source + 'scss/**/*', ['theme-styles']);
  gulp.watch(source + 'js/plugins/*.js', ['theme-plugins-js']);
  gulp.watch(source + 'js/theme.js', ['theme-js']);
})

// default task
gulp.task('default', ['theme-styles', 'theme-plugins-js', 'theme-js', 'shopify', 'watch']);
